package site.leva.controller;

import site.leva.web.Request;
import site.leva.web.ViewModel;

public interface Controller {
    ViewModel process(Request req);
}
