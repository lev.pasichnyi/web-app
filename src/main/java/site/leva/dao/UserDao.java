package site.leva.dao;

import site.leva.model.User;

public interface UserDao {

    User addUser(User user);

    User getByToken(String token);

}