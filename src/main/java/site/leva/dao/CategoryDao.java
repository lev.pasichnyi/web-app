package site.leva.dao;

import site.leva.model.Category;

public interface CategoryDao {
    Category getCategoryById(Long id);
}
