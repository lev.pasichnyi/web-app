package site.leva.service;

import site.leva.model.User;
import java.util.Optional;
import javax.servlet.http.Cookie;

public interface UserService {

    Optional<User> authorize(User user);

    Optional<User> addUser(User user);

    Optional<User> findByToken(String token);
}
